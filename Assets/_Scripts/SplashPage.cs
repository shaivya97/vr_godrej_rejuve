using Michsky.UI.Frost;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using DG.Tweening;
public class SplashPage : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    public string URL, fileName;
    public string[] videoURL;
    public GameObject LoadingObject;
    public Image bgImg;
    public TimedAction timedAction;
    bool isFirst;
    // Start is called before the first frame update
    void Start()
    {
        //if (fileName != "")
        //    videoPlayer.url = System.IO.Path.Combine(Application.streamingAssetsPath, fileName);
        //else
        videoPlayer.url = videoURL[0];
        videoPlayer.Prepare();
        isFirst = true;
        //videoPlayer.Prepare();
    }

    public void PlayVideo(int index)
    {
        //if (fileName!="")
        //{
        //    videoPlayer.Play();
        //    timedAction.InvokeImmediate();
        //}
        //else
        //{
        //    StartCoroutine(PlayVideoFromURL(index));
        videoPlayer.url = videoURL[1];
        StartCoroutine(PlayVideoFromURL(index));

    }

    IEnumerator PlayVideoFromURL(int index)
    {
        if (!isFirst)
        {
            videoPlayer.url = videoURL[index];
            videoPlayer.Prepare();
        }

        isFirst = false;

        LoadingObject.SetActive(true);
        //yield return videoPlayer.isPrepared;
        while (!videoPlayer.isPrepared)
            yield return null;
        videoPlayer.Play();
        while (!videoPlayer.isPlaying)
            yield return null;
        LoadingObject.SetActive(false);
        bgImg.DOFade(0, 0.5f);
        timedAction.InvokeImmediate();
    }

    public void PlayAt(int sec)
    {
        videoPlayer.time = sec;
    }


}
