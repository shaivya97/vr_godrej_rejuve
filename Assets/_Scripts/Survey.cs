﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


public class Survey : MonoBehaviour
{

    [SerializeField] InputField feedback1;
    [SerializeField] InputField feedback2;
    [SerializeField] Text wrongtext;

    string URL = "https://docs.google.com/forms/u/0/d/e/1FAIpQLScmnmc_b8cBcU62fhNZ7cryeG868yPJpdDXHzomne6ywLpwLw/formResponse";
    bool submitted = false;
    public GameObject contact;
    public GameObject thankyoutext;
    public GameObject Form;

    public void Send()
    {
        if(feedback1.text=="" || feedback2.text == "")
        {
            wrongtext.text = "Plese provide the details!";
        }
        else if(feedback1.text == "" && feedback2.text == "")
        {
            wrongtext.text = "Plese provide the details!";
        }
        else
        {
            StartCoroutine(Post(feedback1.text, feedback2.text));
            wrongtext.text = "";
            submitted = true;
            FormHide();
        }
        
    }

    IEnumerator Post(string s1, string s2)
    {
        WWWForm form = new WWWForm();
        form.AddField("entry.301888208", s1);
        form.AddField("entry.1484931695", s2);
        UnityWebRequest www = UnityWebRequest.Post(URL, form);

        yield return www.SendWebRequest();

    }

    public void checksubmit()
    {
        if (submitted == true)
        {
            thankyoutext.SetActive(true);
            contact.SetActive(false);
        }

        else
        {
            thankyoutext.SetActive(false);
            contact.SetActive(true);
        }
    }


    public void FormShow()
    {
        Form.SetActive(true);
    }

    public void FormHide()
    {
        Form.SetActive(false);
    }



}