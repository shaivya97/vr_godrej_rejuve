﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System.Runtime.InteropServices;
using UnityEngine.Video;

public class Controller : MonoBehaviour
{
    public static Controller instance;
   
    [DllImport("__Internal")]
    private static extern void openWindow(string url, bool isDownload);
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        //isMainMenu = false;
    }

    public void OpenWebURL(string url)
    {
#if !UNITY_EDITOR
        openWindow(url, false);
        //StartCoroutine(OpenNewTab(url));
#else
        Application.OpenURL(url);
#endif
    }

    public void DownloadWebURL(string url)
    {
#if !UNITY_EDITOR
        openWindow(url, false);
        //StartCoroutine(OpenNewTab(url));
#else
        Application.OpenURL(url);
#endif
    }



    IEnumerator OpenNewTab(string url)
    {
        yield return new WaitForEndOfFrame();
        openWindow(url, false);
    }
}
