using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class videoandvolumecontrol : MonoBehaviour
{
    public VideoPlayer video;
    //public AudioSource audio;
    public Slider slider;

    public void audiovolume()
    {
        video.SetDirectAudioVolume(0, slider.value);
    }

    public void videoPlay()
    {
        video.Play();
    }

    public void vediopause()
    {
        video.Pause();
    }
}
