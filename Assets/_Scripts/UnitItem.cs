using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class UnitItem : MonoBehaviour
{
    public TextMeshProUGUI unitName;

    public void BookUnit(string url)
    {
        Application.ExternalEval("window.open('" + url + "', '_blank')");
    }
}
