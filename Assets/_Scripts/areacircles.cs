using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class areacircles : MonoBehaviour
{
    public Slider slider;
    public GameObject circle1;
    public GameObject circle2;
    public GameObject circle3;
    public float alpha;


    public void OnChange()
    {
        if (slider.value >= 0 && slider.value <= 2.0f) //circle1 scale
        {
            alpha = slider.value;
            circle1.transform.localScale = new Vector3(alpha, alpha, alpha);
        }

        if (slider.value >= 0 && slider.value <= 3.0f) //circle1 scale
        {
            alpha = slider.value;
            circle2.transform.localScale = new Vector3(alpha, alpha, alpha);
        }

        if (slider.value >= 0 && slider.value <= 4.0f) //circle1 scale
        {
            alpha = slider.value;
            circle3.transform.localScale = new Vector3(alpha, alpha, alpha);
        }
    }
}
