using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class VirtualWalkthroughManager : MonoBehaviour
{
    public GameObject BG;
    public SpriteRenderer darkBG;
    public Camera mainCam;
    bool isTransitioning;
    int currentPage;
    public GameObject[] panaromaImages;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
       // BG.SetActive(false);
        BG.transform.localScale = Vector3.zero;
    }

    private void OnDisable()
    {
        BG.transform.localScale = Vector3.one;
        //BG.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SelectPage(int pageNo)
    {
        if (!isTransitioning)
        {
            mainCam.DOFieldOfView(30, 0.5f).SetEase(Ease.Linear);
            darkBG.DOFade(0.7f, 0.5f).OnComplete(() => {
                mainCam.fieldOfView = 90;
                panaromaImages[currentPage].SetActive(false);
                currentPage = pageNo;
                panaromaImages[currentPage].SetActive(true);
                mainCam.DOFieldOfView(60, 0.5f).SetEase(Ease.Linear);
                darkBG.DOFade(0, 0.5f).OnComplete(() =>
                {
                    isTransitioning = false;
                });
            });
            isTransitioning = true;
        }
    }
}
