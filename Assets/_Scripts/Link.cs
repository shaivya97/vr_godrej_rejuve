﻿using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.UI;

public class Link : MonoBehaviour 
{

	public void OpenLinkJSPlugin(string link)
	{
		#if !UNITY_EDITOR
		openWindow(link);
		#endif
	}

	[DllImport("__Internal")]
	private static extern void openWindow(string url);

}