﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;
using UnityEngine.Events;
using UnityEngine.UI;

public class PressHandler2 : MonoBehaviour, IPointerDownHandler
{
	[Serializable]
	public class ButtonPressEvent : UnityEvent { }

	public ButtonPressEvent OnPress = new ButtonPressEvent();
	bool isClicked;
	public void OnPointerDown(PointerEventData eventData)
	{
		GetComponent<RedirectButton>().Redirect();
		//		OnPress.Invoke();
	}

}