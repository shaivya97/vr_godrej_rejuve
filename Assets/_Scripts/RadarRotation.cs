using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadarRotation : MonoBehaviour
{
    public GameObject walkthrough_camera;
    // Start is called before the first frame update

    void Update()
    {
        //Rotate the pointer according to the camera angle and updates it simultaneously
       float camera_angle_y = walkthrough_camera.transform.rotation.eulerAngles.y;
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, -camera_angle_y);
    }
}
