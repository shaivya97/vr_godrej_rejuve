using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lookat : MonoBehaviour
{
    public Transform cameraTransform;
    // Update is called once per frame
    void Update()
    {
        if (cameraTransform)
            this.transform.LookAt(cameraTransform.position);
        else
            transform.LookAt(Vector3.zero);
    }
}
