﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedirectButton : MonoBehaviour
{
    public string redirectionURL;
    public bool isDownload;
    public void Redirect()
    {
        if (!isDownload)
            Controller.instance.OpenWebURL(redirectionURL);
        else
            Controller.instance.DownloadWebURL(redirectionURL);
    }
}