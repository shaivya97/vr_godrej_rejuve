using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class UnitsManager : MonoBehaviour
{
    public List<Block> BlockList;
    public List<UnitItem> unitsDisplayed;
    public TMP_Dropdown blocksDropDown, floorDropDown;
    public static bool isUnitSelected;
    // Start is called before the first frame update
    void Start()
    {
        SetUnits();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetUnits()
    {
        isUnitSelected = true;
        for (int i = 0; i < 14; i++)
        {
            if(i<9)
                unitsDisplayed[i].unitName.text = BlockList[blocksDropDown.value].blockName + " - " + floorDropDown.value + "0" + (i+1);
            else
                unitsDisplayed[i].unitName.text = BlockList[blocksDropDown.value].blockName + " - " + floorDropDown.value + (i + 1);
        }
        Invoke("TimerOff",1);
    }

    void TimerOff()
    {
        isUnitSelected = false;
    }


}

[System.Serializable]
public class Block{
    public string blockName;
    public List<Floor> floors = new List<Floor>();
}

public class Floor{
    public int floorNo;
    public List<string> units;
}
