using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FloatingButton : MonoBehaviour
{
    [Serializable]
    public class ButtonPressEvent : UnityEvent { }

    public ButtonPressEvent OnPress = new ButtonPressEvent();
    public Transform cameraTransform;
    // Start is called before the first frame update
    void Start()
    {
        if (cameraTransform)
            transform.LookAt(cameraTransform.position);
        else
           transform.LookAt(Vector3.zero);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        OnPress.Invoke();
    }
}
