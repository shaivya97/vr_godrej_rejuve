using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitManager : MonoBehaviour
{
    public GameObject BG;
    private void OnEnable()
    {
        // BG.SetActive(false);
        BG.transform.localScale = Vector3.zero;
    }

    private void OnDisable()
    {
        BG.transform.localScale = Vector3.one;
        //BG.SetActive(true);
    }
}
