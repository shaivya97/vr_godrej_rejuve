using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class fadeimage : MonoBehaviour
{
    public Slider slider;
    public Image night;
    public Image dusk;
    public Image day;
    public float alpha;


    public void OnChange()
    {
        if(slider.value>=0 && slider.value <= 35.0f)
        {
            alpha = slider.value / 35.0f;
            day.color = new Color(day.color.r, day.color.g, day.color.b, alpha);
        }

        else if (slider.value >=35.0f && slider.value <= 70.0f)
        {
            alpha = (slider.value - 35.0f) / (70.0f - 35.0f);
            dusk.color = new Color(dusk.color.r, dusk.color.g,dusk.color.b, alpha);
        }

        else if (slider.value > 70.0f)
        {
            alpha = (slider.value - 70.0f) / (100.0f - 70.0f);
            night.color = new Color(night.color.r, night.color.g, night.color.b, alpha);
        }
    }
  
}
