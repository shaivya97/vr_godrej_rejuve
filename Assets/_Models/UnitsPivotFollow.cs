using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class UnitsPivotFollow : MonoBehaviour
{
    [SerializeField]
    public Transform followPivot;
    public Vector3 resetPosition;
    // Start is called before the first frame update
    void Start()
    {
        //resetPosition = transform.localPosition + new Vector3(0, 200, 0);
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position = followPivot.position;
    }

    public void Reset()
    {
        transform.localPosition = resetPosition;
    }

    public void SetFloor()
    {
        transform.DOMove(followPivot.position, 0.5f);
    }
}
