using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class ScaleUnit : MonoBehaviour
{
    public float multiplier, scaleVal;
    public TMP_Dropdown floorDropDown;
    Vector3 targetScale;
    public UnitsPivotFollow selectedFloor;
    void Start()
    {
        scaleVal = transform.localScale.z;
        ResetScale();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetScale(int val)
    {
        selectedFloor.Reset();
        if (multiplier < 0)
        {
            targetScale = new Vector3(-1, -1, scaleVal * val * multiplier);
            //selectedFloor.localScale = new Vector3(0, -1, 0);
        }
        else
        {
            targetScale = new Vector3(1, 1, scaleVal * val * multiplier);
            //selectedFloor.localScale = new Vector3(1, 1, 0);
        }
        transform.DOScale(targetScale, 0.7f).OnComplete(() =>
        {
            selectedFloor.SetFloor();
            //if (multiplier < 0)
            //{
            //    targetScale = new Vector3(-1, -1, scaleVal);
            //}
            //else
            //{
            //    targetScale = new Vector3(1, 1, scaleVal);
            //}
            //selectedFloor.DOScale(targetScale, 0.5f);
        });
    }

    void ResetScale()
    {
        if (multiplier < 0)
            targetScale = new Vector3(-1, -1, 0);
        else
            targetScale = new Vector3(1, 1, 0);

        transform.localScale = targetScale;
        selectedFloor.Reset();
        selectedFloor.SetFloor();
    }
}
